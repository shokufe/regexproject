import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * cette classe représente notre automate finis deterministe 
 * qui est obtenue a partir de l'automate non deterministe NFA
 * @author kderb,simab
 *
 */

public class DFA2{
	static Automate autom=new Automate();
	/**
	 * cette map represente la structure de données ou on enregistre notre DFA
	 */
	static Map<ArrayList<Integer>,ArrayList< Map<ArrayList<Character>,ArrayList<Integer>>>> transitions;
	/**
	 * represente les etats finaux de notre DFA qui sont utilie pour réaliser le matching
	 */
	static Map<ArrayList<Integer>, Boolean> finalsForMatching=new HashMap<ArrayList<Integer>, Boolean>();
	/**
	 * represente la source de début de matching
	 */
	static Map<ArrayList<Integer>, Boolean> StartPoint=new HashMap<ArrayList<Integer>, Boolean>();
	/**
	 * epsilon 
	 */
	static Character epsilon = +1;
	/**
	 * les etats enregistre tous les etats sources de notre NFA 
	 * qui sont ensuite transformé à un seul etat initiale pour notre DFA
	 */
	ArrayList<Integer> etats=new ArrayList<Integer>();
	/**
	 * Initiale est notre etat initiale de DFA
	 * il est representé comme une liste mais il desingne un seul etat initiale
	 */
	ArrayList<Integer> Initiale=new ArrayList<Integer>();
	ArrayList<Integer> Initiale1=new ArrayList<Integer>();
	/**
	 * finaux c'est la liste de tous les etats finaux 
	 */
	ArrayList<Integer> finaux=new ArrayList<Integer>();
	ArrayList<Integer> sources=new ArrayList<Integer>();
	/**
	 * cette liste enregiste les etats finaux de notre NFA
	 */
	ArrayList<Integer> finals=new ArrayList<Integer>();
	/**
	 * cette liste permet d'enregister l'etat de debut de concat
	 */
	ArrayList<Integer> concatSources=new ArrayList<Integer>();
	/***
	 * ces trois liste servent a enregister les caracteres obtenue de NFA
	 */
	ArrayList<Character> charList=new ArrayList<Character>();
	ArrayList<Character> charListEtoile=new ArrayList<Character>();
	ArrayList<Integer> epsilonListEtoile=new ArrayList<Integer>();
	ArrayList<Integer> etoileSources=new ArrayList<Integer>();
	/**
	 * cette liste permet d'enregistrer les caracteres de DFA
	 */
	ArrayList<Character> v=new ArrayList<Character>();
	/**
	 * ces deux listes servent a enregistrer les etats finaux de DFA ET NFA
	 */
	ArrayList<Integer> finauxC=new ArrayList<Integer>();
	ArrayList<Integer> finauxConcat=new ArrayList<Integer>();
	/**
	 * cette liste permet d'enregistrer l'etat finale de ALTERN
	 */
	ArrayList<Integer> finauxAlt=new ArrayList<Integer>();
	/**
	 * cette liste permet d'enregistrer l'etat initiale de ALTERN
	 */
	ArrayList<Integer> sourceAlt=new ArrayList<Integer>();
	ArrayList<Integer> IniConcat=new ArrayList<Integer>();
	ArrayList<Integer> help=new ArrayList<Integer>();
	static Integer nrBranchesAlternes=0;
	static Integer nrBranchesetat=0;

	int size=0;
	ArrayList<Map<ArrayList<Character>,ArrayList<Integer>>>liste=new ArrayList<Map<ArrayList<Character>,ArrayList<Integer>>>();
	boolean altern=false;
	boolean concat=false;
	boolean etoile=false;
	boolean finConcat=false;
	boolean repeat=false;
	boolean repeat1=false;
	boolean passage=false;

	/**
 * cette methode permet de supprimer les redandant dans la liste
 * @param liste2
 * @return
 */


	public static ArrayList<Integer> removeredandant(ArrayList<Integer>liste2){

		for(int j=0;j<liste2.size();j++) {
			
			for(int k=1;k<=liste2.size()-1;k++) {
				
				if(liste2.get(j).equals(liste2.get(k))) {
					liste2.remove(k);
				}
			}
		}
		
		return liste2;
		
	}
	/**
	 * cette méthode traite l'automate non deterministe es 
	 * elle permet de le détérminiser
	 * @param es :ce parametre est notre automate non détérministe
	 */

	public  void setautomateDFA(Map<Integer, Map<Character, ArrayList<Integer>>> es) {
		NFA2 nfa=new NFA2();
		finals=nfa.finals;
		transitions=new HashMap<ArrayList<Integer>,ArrayList< Map<ArrayList<Character>,ArrayList<Integer>>>>();
		if(!es.isEmpty()) {
			for (int i=0;i<=es.size()-1;i++) {
				/**traitement des epsilons transitions****/
				if(es.get(i).containsKey(epsilon) ) {
					ArrayList<Integer>essaie=new ArrayList<Integer>();
					essaie.addAll(es.get(i).get(epsilon));
					
					essaie=removeredandant(essaie);
					
					
						
					/** cas de altern sans concat ni etoile **************************/
					if(es.get(i).get(epsilon).size()>=2 && !concat && !etoile) {
						size=es.get(i).get(epsilon).size();
						altern=true;
						Initiale.add(i);
						Initiale.addAll(es.get(i).get(epsilon));
							/** etat permet d'enregistrer tous les etats sources**/
						etats.addAll(es.get(i).get(epsilon));
						/**tous ces etat sources de NFA enregistré dans etat vont etre transformé a une liste
						 * mais qui represente un seule etat source pour le DFA
						 */
						if(etats.size()>1 ) {
							for(int i1=0;i1<etats.size();i1++) {
								if(es.get(etats.get(i1))!=null) {
									if(es.get(etats.get(i1)).containsKey(epsilon)) {
										if(!Initiale.containsAll(es.get(etats.get(i1)).get(epsilon)))	
											Initiale.addAll(es.get(etats.get(i1)).get(epsilon));
									}
								}}
						}
						/*** 
						 * cas de ALTERN avec Concat et not Etoile
						 * 
						 * 
						 * ****/
					}else if(es.get(i).get(epsilon).size()>=2 && concat && !etoile  ) {
						
						if(es.get(i).get(epsilon).size()>=2) {
							size=es.get(i).get(epsilon).size();
						altern=true;
						Initiale.add(i);
						Initiale.addAll(es.get(i).get(epsilon));
						etats.addAll(es.get(i).get(epsilon));
						/**tous ces etat sources de NFA enregistré dans etat vont etre transformé a une liste
						 * mais qui represente un seule etat source pour le DFA
						 */
						if(etats.size()>1 ) {
							for(int i1=0;i1<etats.size();i1++) {
								if(es.get(etats.get(i1))!=null) {
									if(es.get(etats.get(i1)).containsKey(epsilon)) {
										if(!Initiale.containsAll(es.get(etats.get(i1)).get(epsilon)))	
											Initiale.addAll(es.get(etats.get(i1)).get(epsilon));
									}
								}}
						}
					   
						}	
						
					
						
						
					}else if(es.get(i).get(epsilon).size()>=2 && !concat && es.get(i).size()<2) {
						for (Entry<Character, ArrayList<Integer>> entry1 : es.get(i).entrySet()) {
							
							if(entry1.getValue().size()>=2) {
							if(finals.contains(entry1.getValue().get(1))) {
								passage=true;
							}else {
								passage=false;
							}
							}
						}
						if(!passage) {
						if(es.get(i).get(epsilon).size()>=2) {
							size=es.get(i).get(epsilon).size();
						altern=true;
						Initiale.add(i);
						Initiale.addAll(es.get(i).get(epsilon));
						etats.addAll(es.get(i).get(epsilon));
						/**tous ces etat sources de NFA enregistré dans etat vont etre transformé a une liste
						 * mais qui represente un seule etat source pour le DFA
						 */
						if(etats.size()>1 ) {
							for(int i1=0;i1<etats.size();i1++) {
								if(es.get(etats.get(i1))!=null) {
									if(es.get(etats.get(i1)).containsKey(epsilon)) {
										if(!Initiale.containsAll(es.get(etats.get(i1)).get(epsilon)))	
											Initiale.addAll(es.get(etats.get(i1)).get(epsilon));
									}
								}}
						}
					   
						}	
						}
					}
					

					else if(concat && altern && !etoile ) {
						//concatSources=new ArrayList<Integer>();
						concatSources.add(i);
						for(int k=0;k<=Initiale.size()-1;k++) {
							if(Initiale.get(k).equals(i))
								Initiale.remove(k);
						}
						if(concatSources.size()>=etats.size()-1 && concatSources.size()>=2) {
							altern=false;
						}

						
						
					}
						/***
					 * cas de altern et etoile et not concat
					 * sans ce cas on  parcours l'automate qui traite le altern et etoile
					 * et elle assure la recursivité 
					 * 
					 * ****/
					
					else if(!concat && altern && etoile) {
						
							concatSources.add(i);
						for(int k=0;k<=Initiale.size()-1;k++) {
							if(Initiale.get(k).equals(i))
								Initiale.remove(k);
						}
						
						if(es.get(i).size()<2 ) {
						v=new ArrayList<Character>();
						v.addAll(charListEtoile);
						if(!v.isEmpty()) {
						epsilonListEtoile=new ArrayList<Integer>();
						epsilonListEtoile.add(concatSources.get(concatSources.size()-1));
						sourceAlt=new ArrayList<Integer>();
						sourceAlt.add(nfa.sourcesAltern.get(0)-1);
						add(epsilonListEtoile,sourceAlt);
						}}else {
						concatSources.remove(concatSources.get(concatSources.size()-1));
							v=new ArrayList<Character>();
							for (Entry<Character, ArrayList<Integer>> entry1 : es.get(i).entrySet()) {
							if(!entry1.getKey().equals(epsilon)) {
								v.add(entry1.getKey());
								finaux=new ArrayList<Integer>();
								finaux.addAll(entry1.getValue());
							}
							
							}
								for(int j=0;j<concatSources.size();j++) {
									epsilonListEtoile=new ArrayList<Integer>();
									epsilonListEtoile.add(concatSources.get(j));
									add(epsilonListEtoile,finaux);
								}
								
								sourceAlt=new ArrayList<Integer>();
								sourceAlt.add(nfa.sourcesAltern.get(0)-1);
								add(sourceAlt,finaux);
								System.out.println("transitionsss"+transitions);
							
								
							
							
							
						}
						if( concatSources.size()>=etats.size()-1 && concatSources.size()>=2) {
							altern=false;
							concat=true;
							etoile=false;
							concatSources=new ArrayList<Integer>();
							concatSources.addAll(finaux);
						}
						charList=new ArrayList<Character>();
						v=new ArrayList<Character>();
						charListEtoile=new ArrayList<Character>();
						
						
						
					}
					
					else if( !concat && !etoile && altern) { 
						if(finals.contains(i)) {
							for (Entry<Character, ArrayList<Integer>> entry : es.get(i).entrySet()) {
								if(entry.getKey()!=epsilon) {
									v.add(entry.getKey());
									finaux=new ArrayList<Integer>();
									finaux.addAll(entry.getValue());
									add(sources, sources);
								}else {
								}
							}
						}else {
							for (Entry<Character, ArrayList<Integer>> entry : es.get(i).entrySet()) {
								if(entry.getKey()!=epsilon) {
									v.add(entry.getKey());
									finaux=new ArrayList<Integer>();
									finaux.addAll(entry.getValue());
									add(sources, finaux);

								}else {
								}
							}
						}
					}
					/*******************fin de cas altern *********/
					else if(!altern ) {
						for (Entry<Character, ArrayList<Integer>> entry1 : es.get(i).entrySet()) {
							if(!etoile && es.get(i).get(epsilon)!=null ) {
								if(finals.contains(es.get(i).get(epsilon).iterator().next())||NFA2.sourcesAltern.contains(es.get(i).get(epsilon).iterator().next())) {
									etoile=true;
									concat=false;
								}else {
									concat=true;
									//etoile=false;
								}
							}	}
							/**
						 * cas de etoile sans concat et sans altern
						 * ce cas traite le regex avec juste une etoile
						 * et assure la recursivite
						 */
						if(etoile|| i==0 && !concat) {
							if(es.get(i).get(epsilon).size()>=1 && !finals.contains(i)) {
								if(Initiale.isEmpty()) {
									Initiale.add(i);
									Initiale.addAll(es.get(i).get(epsilon));
									concat=true;
								}
							}
							/***** dans ce cas on est dans etoile mais juste avec des parentheses
							 * sans le concat avec d'autres caracteres  *****/
							else if(es.get(i).size()>=2 && es.get(i).get(epsilon).size()==1 && finals.contains(i) ) {
								for (Entry<Character, ArrayList<Integer>> entry : es.get(i).entrySet()) {
									if(!entry.getKey().equals(epsilon))
										v.add(entry.getKey());
								}
								if(!Initiale.isEmpty()) {
									addEtoile(v, Initiale, Initiale);
								}else {
									addEtoile(v, finaux, finaux);
								}
								   
							}else if(es.get(i).get(epsilon).size()>=2 && finals.contains(i) ) {
								v=new ArrayList<Character>();
								for (Entry<Character, ArrayList<Integer>> entry : es.get(i).entrySet()) {
									if(!entry.getKey().equals(epsilon))
										v.add(entry.getKey());
								}
									finaux=new ArrayList<Integer>();
									finaux.add(i-1);
										if(!v.isEmpty())
											add(Initiale, finaux);
										
								
								
							}
						}
						else if(!etoile && concat) {
							if(Initiale.isEmpty()) {
								Initiale.add(i);
								//Initiale.addAll(es.get(i).get(epsilon));
								concat=true;
							}else {
								for (Entry<Character, ArrayList<Integer>> entry : es.get(i).entrySet()) {
									if(!Initiale.contains(entry.getValue()) && !finals.contains(i))
										//Initiale.addAll(entry.getValue());
										Initiale.add(i);
								}
							}
						}
					}
					if( !Initiale.isEmpty()) {
						for(int j=0;j<=Initiale.size()-1;j++) {
						if(finals.contains(Initiale.get(j)) )
							Initiale.remove(Initiale.get(j));
					}
					}
						
				}

				/*** 
				 * 
				 * dans ce cas on traite pas les epsilons transitions
				 * mais les transitions avec des caracteres
				 * tels que v est la liste ou on enregiste les caracteres de notre DFA
				 */
				else {
					if(v.isEmpty()) {
						v.add(es.get(i).keySet().iterator().next());
						finaux=new ArrayList<Integer>();
						finaux.addAll(es.get(i).get(v.get(0)));
					}else {
						v.add(es.get(i).keySet().iterator().next());
						finaux=new ArrayList<Integer>();
						for (Entry<Character, ArrayList<Integer>> entry : es.get(i).entrySet()) {
							finaux.addAll(entry.getValue());
						}
					}
					/**
					 * dans le cas ou on vient de altern apres avoir elminer les epsilons transitions
					 */
					if(altern && size>=2 ){
					if(!finauxC.isEmpty()) {
						if(Initiale.contains(finauxC.get(0))) {
							Initiale=new ArrayList<Integer>();
							Initiale.addAll(finauxC);
						}
					}
						sources.addAll(Initiale);
						
						if(!transitions.isEmpty() ) {
							if(transitions.get(sources)!=null) {
						      for(int k=0;k<=transitions.get(sources).size()-1;k++) {
							  if(!transitions.get(sources).get(k).containsKey(v)) {
								  add(sources,finaux);
							     repeat1=true;
							     
							}else {
								repeat=true;
							}
								
						 }
						}else {
							add(sources,finaux);
						}
							
					}if(transitions.isEmpty()) {
						add(sources,finaux);
						finauxAlt.addAll(finaux);
					}
					if(etoile) {
						charListEtoile=new ArrayList<Character>();
						charListEtoile.addAll(v);
					}
						
					 v=new ArrayList<Character>();
					   Initiale=new ArrayList<Integer>();
						for (Entry<Character, ArrayList<Integer>> entry : es.get(i).entrySet()) {

							nrBranchesAlternes++;
							if(finals.contains(entry.getValue().get(0)) && concat ){
								altern=false;	
								concat=true;
								

							}else if(!es.get(entry.getValue().get(0)).containsKey(epsilon)){
								concat=true;
								altern=false;
							}
							
						}
						if(altern) {
							//Initiale=new ArrayList<Integer>();
							//	v=new ArrayList<Character>();
						}
						
					
					}
					/**
					 * dans le cas ou on vient de concat et pas etoile
					 *
					 */
					if(concat && !etoile && concatSources.isEmpty()) {
						Initiale1=new ArrayList<Integer>();
						Initiale1.addAll(removeredandant(Initiale));
						
						for (Entry<Character, ArrayList<Integer>> entry : es.get(i).entrySet()) {
							entry.getValue();
							if(finals.contains(es.get(entry.getValue().get(0)))) {
								finaux.addAll(es.get(entry.getValue().get(0)).get(epsilon));
								for(int j=0;j<finals.size();j++) {
									if(!finaux.contains(finals.get(j))) {
										finaux.add(finals.get(j));
									}
								}
								add(Initiale1,finaux);
							}else { 
								finauxC.addAll(finaux);
								if(finals.contains(finaux.get(0)) && es.get(finaux.get(0)).get(epsilon).size()!=2) {
									altern=true;
									concat=false;
								}else if(finals.contains(finaux.get(0)) && es.get(finaux.get(0)).get(epsilon).size()==2) {
									etoile=true;
									concat=false;
								}
								IniConcat=new ArrayList<Integer>();
								
								if(!Initiale.isEmpty()) {
									IniConcat.add(Initiale.get(0));
									if(!repeat) {
										add(IniConcat,finaux);
										Initiale=new ArrayList<Integer>();
										
										Initiale.addAll(finaux);
									}else {
										if( repeat && !repeat1) {
											add(finauxAlt,finaux);

										}else if(!concat){
											add(Initiale,finaux);
											
										}else {
											add(Initiale,finaux);
										}
										Initiale=new ArrayList<Integer>();
										
										Initiale.addAll(finaux);
										
									}
									

								}else {
									Initiale=new ArrayList<Integer>();
									Initiale.addAll(finaux);
								}
							}
						}
						v=new ArrayList<Character>();
						finConcat=true;
					}
					/**
					 * dans le cas ou on vient de concat et pas altern
					 * dans ce cas on a besoin des etats sources de notre concat 
					 * qui recuperer a partir du NFA
					 */
					if(concat && !altern && !concatSources.isEmpty()) {
						//concatSources.add(i);
						
						for(int k=0;k<=concatSources.size()-1;k++) {
							ArrayList<Integer>newliste=new ArrayList<Integer>(); 
							newliste.add(concatSources.get(k));
							add(newliste,finaux);
						}
					//	concatSources.remove(0);
						
						
						concatSources=new ArrayList<Integer>();
						v=new ArrayList<Character>();
						
						
						
					}
					/**
					 * le cas de etoile ici on traite la recursivité de caractere 
					 * apres avoir supprimer les epsilons transitions
					 */
					if(etoile ) {
						for (Entry<Character, ArrayList<Integer>> entry : es.get(i).entrySet()) {
                    if(transitions.get(Initiale)!=null) {
						if(transitions.get(Initiale).get(0).size()>=1) {
							transitions.clear();
						}}
                    if(!v.isEmpty()) {
						sources=new ArrayList<Integer>();
						sources.addAll(Initiale);
						addEtoile(v, sources, sources);
						}
						}
                    
					}
				}
			}
		}
	}
		/**
	 * cette methode permet d'ajouter les transition qui viennet de ALTERN ET CONCAT
	 * apres avoir supprimer les epsilons transitions
	 * @param sources:c'est l'etat sources de notre DFA representé comme une liste
	 * car il contient tous les etat initiaux de NFA apres avoir supprimer les epsilons transtins 
	 * mais il represente un seule etat source ppour notre DFA
	 * @param sfinaux: represente les etats finaux de notre DFA
	 */
	public void add(ArrayList<Integer> sources,ArrayList<Integer> sfinaux) {
		if(!transitions.keySet().contains(sources)) {
			transitions.put(sources, new ArrayList<Map<ArrayList<Character>,ArrayList<Integer>>>());
			Map<ArrayList<Character>, ArrayList<Integer>>mp=new HashMap<ArrayList<Character>, ArrayList<Integer>>();
			mp.put(v, new ArrayList<Integer>());
			mp.get(v).addAll(sfinaux);
			liste=new ArrayList<Map<ArrayList<Character>,ArrayList<Integer>>>();
			liste.add(mp);
			transitions.get(sources).addAll(liste);
		}else {
			Map<ArrayList<Character>, ArrayList<Integer>>mp=new HashMap<ArrayList<Character>, ArrayList<Integer>>();
			mp.put(v, new ArrayList<Integer>());
			mp.get(v).addAll(sfinaux);
			liste=new ArrayList<Map<ArrayList<Character>,ArrayList<Integer>>>();
			liste.add(mp);
			transitions.get(sources).addAll(liste);

		}
		//finalsForMatching=new HashMap<ArrayList<Integer>, Boolean>();
		finalsForMatching.replace(help, false);
		for(int i=0;i<liste.size();i++) {
			Map<ArrayList<Character>,ArrayList<Integer>> f=liste.get(i);
			for (Entry<ArrayList<Character>, ArrayList<Integer>> entry : f.entrySet()) {
				if(!finals.isEmpty()) {
					if(finals.contains(entry.getValue().get(0)))
					{
						finalsForMatching.put(entry.getValue(), true);
					}
					else {
						finalsForMatching.put(entry.getValue(), false);
					}
			}
			}
		}
		
		if(sources.contains(autom.geteSource()))
			StartPoint.put(sources, true);
	}
	/**
	 * cette methode permet d'ajouter les transition qui viennet de AEOILE
	 * elle assure la recursivité dans caracteres
	 * apres avoir supprimer les epsilons transitions
	 * @param sources:c'est l'etat sources de notre DFA representé comme une liste
	 * car il contient tous les etat initiaux de NFA apres avoir supprimer les epsilons transtins 
	 * mais il represente un seule etat source ppour notre DFA
	 * @param sfinaux: represente les etats finaux de notre DFA
	 * @param charliste: les caracteres a qui on doit assurer la recursivité
	 */
	public void addEtoile(ArrayList<Character> charliste,ArrayList<Integer> sources,ArrayList<Integer> sfinaux) {
		if(!transitions.keySet().contains(sources)) {
			transitions.put(sources, new ArrayList<Map<ArrayList<Character>,ArrayList<Integer>>>());
			Map<ArrayList<Character>, ArrayList<Integer>>mp=new HashMap<ArrayList<Character>, ArrayList<Integer>>();
			mp.put(charliste, new ArrayList<Integer>());
			mp.get(charliste).addAll(sfinaux);
			liste=new ArrayList<Map<ArrayList<Character>,ArrayList<Integer>>>();
			liste.add(mp);
			transitions.get(sources).addAll(liste);
		}else {
			Map<ArrayList<Character>, ArrayList<Integer>>mp=new HashMap<ArrayList<Character>, ArrayList<Integer>>();
			mp.put(charliste, new ArrayList<Integer>());
			mp.get(charliste).addAll(sfinaux);
			liste.add(mp);
			transitions.get(sources).addAll(liste);
		}
		
		for(int i=0;i<sources.size();i++) {
			
			//System.out.println("finaux"+finaux +"finals"+finals );
			//if(finaux.contains(sources.get(i))&& finals.contains(sources.get(i))) {
				finalsForMatching.put(sources, true);
				help.addAll(sources);
			//}
			System.out.println("finalformatching"+finalsForMatching);
		}
		if(sources.contains(autom.geteSource()))
			StartPoint.put(sources, true);
	}


}