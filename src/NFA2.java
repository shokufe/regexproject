import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;



import java.util.Map.Entry;

/***
 * cette classe represente notre automate non deterministe 
 * avec les epsilon transitions
 * et les transitions qui consomment des caracteres
 * @author kderb,Simab
 *
 */
public class NFA2 {
	/**
	 * Ces caracteres représentent les caracteres speciaux de notre AST
	 */
	static final int CONCAT = 0xC04CA7;//12602535
	static final int ETOILE = 0xE7011E; 
	static final int ALTERN = 0xA17E54;
	static final int PROTECTION = 0xBADDAD;
	static final int PARENTHESEOUVRANT = 0x16641664;
	static final int PARENTHESEFERMANT = 0x51515151;
	static final int DOT = 0xD07;//3335
	/**
	 * cette variable represente les sous arbres de notre arbre
	 */
	static int nbrSubTree=0;
	/**
	 * cette liste contient tous les etats finaux de notre NFA
	 */
	static ArrayList<Integer> finals=new ArrayList<Integer>();
		/**
	 * cette liste represente les etats initiaux de notre automate
	 */
	static ArrayList<Integer> initials=new ArrayList<Integer>();
	/**
	 * ce caractere représente notre epsilon
	 */
	static Character epsilon = +1;
	char charactere;
	public static final int MAX_CHAR = 255 ;
	/**
	 * représente notre automate principale
	 */
	static Automate autom=new Automate();
	static int base =0;
	static boolean bool=false;
	static int taillesub=0;
	static ArrayList<Integer> liste=new ArrayList<Integer>();
	static ArrayList<Integer> sourcesAltern=new ArrayList<Integer>();	
	static ArrayList<Integer> listeSources=new ArrayList<Integer>();	
	static ArrayList<Character> listeXhar=new ArrayList<Character>();
	static int nbr=0;
	static int alternbr=0;
	static int actuel=0;
	static Integer node=0;
	
	static boolean altern =false;
	static boolean etoile =false;
	static boolean concat=false;
	static boolean nullActuel=false;
	static Integer sourceNull=0;
	public NFA2() {}
	/**
	 * cette methode est utilisé pour la creation de notre NFA 
	 * @param tree: notre AST
	 * @param es:notre NFA
	 * @return
	 */
	public static Automate callMain(RegExTree tree ,Map<Integer, Map<Character, ArrayList<Integer>>> es) {
		Automate aut=new Automate();
		aut=setautomate(tree,es);
		createEtatFinale(es);
		return aut;
	}
	/**
	 * cette methode permet de supprimer les redondants de la liste
	 * @param liste2
	 * @return
	 */
	public static ArrayList<Integer> removeredandant(ArrayList<Integer>liste2){
		ArrayList<Integer> newlist=new ArrayList<Integer>();
		for(Integer elements:liste2) {
			if(!newlist.contains(elements))
				newlist.add(elements);
		}
		
		return newlist;
		
		
	}
	/**
	 * cette methode permet de creer les etats finaux 
	 * @param es: represente notre NFA
	 */
	public static void createEtatFinale(Map<Integer, Map<Character, ArrayList<Integer>>> es) {
		/*if(autom.eActuel !=0) {
			finals.add(autom.geteActuel());
		}*/
		ArrayList<Integer>fin=new ArrayList<Integer>();
		fin.addAll(removeredandant(finals));
		autom.newstate();
		for(int i=0;i<fin.size();i++) {
			epsilonTransitionAdd( fin.get(i), autom.eActuel,es);
		}
	}
		/**
	 * cette methode est la methode principale de la creation de notre NFA a partir du AST
	 * @param tree: est de type REGEXTree et represente notre ast
	 * @param es:represente la structure de données qui enregistre notre NFA
	 * @return
	 */
	public static Automate setautomate(RegExTree tree ,Map<Integer, Map<Character, ArrayList<Integer>>> es) {
		if(bool) {
			return autom;
		}else {
			/**
			 * cas ou la racine de notre racine commence par le caractere special CONCAT
			 * 
			 */
			if(tree.root == CONCAT) {
				/**
				 * creation de la epsilon transition
				 */
				if(altern && nbr>1 && concat) {
					sourceNull=autom.eSource;
					
		
					
							autom.newstate();

                     if(!initials.isEmpty()) {

                    	 for(int i=0;i<initials.size();i++) {

                    		 node=initials.get(i);
                    	 }
						  /**
                * on appelle la methode epsilonTransitionAdd
                * pour la creation de epsilon transition qui coresspond au root
                * cad qui correspon au concat 
                */

                    	 epsilonTransitionAdd(node,autom.eActuel,es);
                    	 actuel=autom.eActuel;
                    	 initials.remove(node);

                    	 finals.add(autom.geteFinal());
                    	 autom.newstate();
                    	 if(autom.eSource <finals.get(finals.size()-1)) {
                    		 epsilonTransitionAdd(autom.eSource,finals.get(finals.size()-1),es);

                    	 }else {
                    		 epsilonTransitionAdd(autom.eSource,autom.eActuel,es);

                    		 
                    	 }
                    }else {

                    	 	
	                    	autom.eActuel=autom.eSource;
	                    	autom.eSource--;
	                    	if(es.get(autom.eSource)==null) {
	                    	epsilonTransitionAdd(autom.eSource,autom.eActuel,es);
                    	
                    	 	}
                    }
                    
                 	
						



				
				}else {
					
					
			     
			   
				autom.newstate();
				epsilonTransitionAdd(autom.eSource,autom.eActuel,es);
				concat=true;
				}
				/**
				 * parcourir tous le subtree 
				 * 
				 */
		 int ii=0;
				for(Integer i=0;i<tree.subTrees.size();i++) {
					savetransition(tree.subTrees.get(i),tree.subTrees.size(),es,i);
					ii=i;
				}
			if(altern  ) {
				
				//if(autom.geteFinal()!=0)
					//finals.add(autom.geteFinal());
				
				
					autom.seteFinal(autom.eActuel);
					
					finals.add(autom.geteFinal());
					
					for(int i=0;i<finals.size();i++) {
						if(es.get(finals.get(i))!=null) {
							finals.remove(i);
						}
					}
					
					
				}
			
			else {
				/*if(etoile)
					bool=true;*/
				autom.seteFinal(autom.eActuel);
				finals.add(autom.geteFinal());
				}
				
			
			}else if(tree.root == ETOILE) {
				/**
				 * creation de la epsilon transition qui correspond a etoile qui est la racine de notre arbre
				 * la methode newState permet d'incrementer le noeud actuel et de donner sa valeur au noeud Source
				 * @param tree
				 */
				etoile=true;
				int automConcat=autom.eActuel;
				autom.newstate();
				epsilonTransitionAdd( autom.eSource, autom.eActuel,es);
				/**
				 * on parcoure tous notre arbre 
				 */
				for(Integer i=0;i<tree.subTrees.size();i++) {
					recursiontransition(tree.subTrees.get(i),tree.subTrees.size(),es);
				}
			
				if(autom.geteFinal() !=0) {
				
					if(altern) {
					}else {
						
						if(!concat)
							finals.add(autom.geteFinal());
						
						System.out.println(finals);
					}
					
				}else {
					autom.seteFinal(autom.eActuel);
					
				}
				
				if(!bool ) {
					if(!concat) {
						
					}else {
						System.out.println("hhh");
						epsilonTransitionAdd( autom.eActuel,base, es);
					}
					
				}
				/**
				 * ici on traite le cas ou notre racine est un ALTERN
				 */
			}else if(tree.root==ALTERN){
				altern=true;
				alternbr++;
				/**
				 * ici on traite le cas ou on est passer deja par une racine qui a etoile
				 */
				if(etoile) {
					sourcesAltern=new ArrayList<Integer>();
					finals=new ArrayList<Integer>();
					sourcesAltern.add(autom.eSource);
				}
				autom.newstate();
				autom.seteInitial(autom.eSource);
				
				if(!initials.contains(autom.geteInitial()))
					initials.add(autom.geteInitial());
				Integer finale=0;		//epsilon transition
				epsilonTransitionAdd( autom.eSource, autom.eActuel,es);
				int ii=0;
				/***
				 * on parcours notre sous arbre
				 */
				for(int i=0;i<tree.subTrees.size();i++) {
					Alterntransition(autom.geteInitial(),tree.subTrees.get(i),tree.subTrees.size(),es,i);
					ii=i;
				}

				if(autom.geteFinal()!=0 && ii==2) {
					if(!finals.contains(autom.geteFinal()) )
						finals.add(autom.geteFinal());
					if(!finals.contains(autom.eActuel) )
						finals.add(autom.eActuel);
				}else {
					if(concat )
						autom.seteFinal(autom.eActuel);
					

				}
				
			
			}
			else  {
				autom.newstate();
				charTransitionAdd(tree, autom.eSource, autom.eActuel,es);
				autom.newstate();

				for(int i=0;i<finals.size();i++) {
					epsilonTransitionAdd(finals.get(i), autom.eActuel,es);
				}
			}
		}
		return autom;
	}
		/**
	 * cette methode est appellés lorsque on a une racine etoile
	 * puis on parcours son subtree 
	 * elle permet d'assurer la recursivité
	 * @param t:c'est notre sous arbre de l'arbre qui a comme racine ETOILE
	 * @param size
	 * @param es: représente notre automate non detreministe
	 */
	private static void recursiontransition( RegExTree t,int size,Map<Integer, Map<Character, ArrayList<Integer>>> es) {
		if(t.subTrees.isEmpty()) {
			autom.newstate();
			autom.seteFinal(autom.eSource);
			charTransitionAdd(t, autom.eSource, autom.eActuel,es);
			epsilonTransitionAdd(autom.eActuel,autom.eSource,es);
			if(autom.geteFinal()!=0)
				finals.add(autom.geteFinal());
			autom.seteFinal(autom.eActuel);
			bool=true;
		}
		/**
		 * ici on assure le cas ou on vient d'un arbre qui comme racine une ETOILE
		 * mais on tombe aussi sur un sous arbre qui une racine qui contient un caractere speciale
		 */
		else {
			
			base=autom.eActuel+1;
			autom.seteFinal(base);
			if(!finals.contains(autom.geteFinal()))
				finals.add(autom.geteFinal());
			/**
			 * le recursivité
			 */
			setautomate(t,es);
		}
	}
		/***
	 * cette methode est appelle lorsque on vient d'un arbre qui a comme racine un ALTERN
	 * @param initiale:represente les etats initiaux de notre arbre 
	 * @param t: representre notre sous arbre
	 * @param size
	 * @param es:represente note NFA
	 * @param taille
	 */
	public static void Alterntransition(Integer initiale, RegExTree t,int size,Map<Integer, Map<Character, ArrayList<Integer>>> es,int taille) {
		if(t.subTrees.isEmpty()) {
			
			if(autom.geteFinal()==0) {
			
				autom.newstate();
				charTransitionAdd(t, autom.eSource, autom.eActuel,es);
				
				if(!finals.contains(autom.eActuel))
					finals.add(autom.eActuel);
				if(initials.size()>=1) {
					autom.newstate();
					epsilonTransitionAdd(initiale, autom.eActuel,es);
					initiale--;
					if(initiale>=0) {
						autom.seteInitial(initiale);
					}
					initials.remove(initials.size()-1);
				}
			
			}else {
				autom.newstate();
				if(!finals.contains(autom.eSource)) {
				 charTransitionAdd(t, autom.eSource, autom.eActuel,es);
					if(!finals.contains(autom.eActuel) && es.get(autom.eActuel)==null)
						finals.add(autom.eActuel);
					if(initials.size()>=1) {
						autom.newstate();
						
						epsilonTransitionAdd(initiale, autom.eActuel,es);
						initiale--;
						if(initiale>=0) {
							autom.seteInitial(initiale);
						}
						initials.remove(initials.size()-1);
					}
							
				
					
				}else {
					
				
				
				if(initials.size()>=1) {
					
					if(etoile && !altern) {
						epsilonTransitionAdd(initiale, autom.eSource,es);
						node=autom.eSource;
						autom.newstate();
						charTransitionAdd(t, node, autom.eActuel, es);
						
					}else {
				
					epsilonTransitionAdd(initiale, autom.eActuel,es);
				
					autom.newstate();
					charTransitionAdd(t, autom.eSource, autom.eActuel, es);
					initials.remove(initials.size()-1);
				}
				}
				if(!finals.contains(autom.eActuel) && es.get(autom.eActuel)==null)
					finals.add(autom.eActuel);
			}
			}
		}
		/**
		 * ici on assure les cas ou on vient d'un arbre qui comme racine un ALTERN
		 * et on tombe sur un sous arbre qui a comme racine un caractere speciale
		 */
		else {
			if(concat)
				autom.seteFinal(autom.eActuel);
			nbr++;
			setautomate(t,es);
		}
	}
	/**
	 * cette methode assure le traitement du sous arbre de l'arbre qui a une racine CONCAT
	 * @param t:c'est notre sous arbre 
	 * @param size
	 * @param es:represente notre NFA
	 * @param taille
	 */
	public static void savetransition( RegExTree t,int size,Map<Integer, Map<Character, ArrayList<Integer>>> es,int taille) {
		
		if(t.subTrees.isEmpty()) {
			if(altern && !concat) {
				createEtatFinale(es);
				finals=new ArrayList<Integer>();
				if(etoile)	{
					epsilonTransitionAdd(sourcesAltern.get(0), autom.eActuel, es);
					epsilonTransitionAdd(autom.eActuel,sourcesAltern.get(0), es);
					etoile=false;
				}
				autom.newstate();
				charTransitionAdd(t, autom.eSource, autom.eActuel,es);
				
					
				if(finals.contains(autom.eSource))
					finals.remove(autom.eSource);
			}else {
				/***ajout ca*////
				if(!etoile && !altern) {
					autom.newstate();
					charTransitionAdd(t, autom.eSource, autom.eActuel,es);
					if(finals.contains(autom.eSource) && !etoile)
						finals.remove(autom.eSource);
					
				}/****/else {
				autom.newstate();
				charTransitionAdd(t, autom.eSource, autom.eActuel,es);
				if(finals.contains(autom.eSource) && !etoile)
					finals.remove(autom.eSource);
				}
			}
				
				
			
			
		}
		/**
		 * dans ce cas on assure si on vient d'un arbre qui a comme racine un Concat
		 * mais on tombe sur un sous arbre sui a comme racine un caractere speciale
		 */
		else {
			if(es.get(autom.eActuel)==null && altern) {
				nullActuel=true;
				autom.seteFinal(autom.eActuel);
			}
			concat=false;
			setautomate(t,es);
		}
	}
	/***
	 * cette methode permet d'ajouter dans notre structure de données une epsilon transition
	 * @param eSource:source de la transition
	 * @param eActuel: la destination de la transition
	 * @param es:notre structure de données qui represente notre NFA
	 */
	public static void epsilonTransitionAdd(Integer eSource,Integer eActuel,Map<Integer, Map<Character, ArrayList<Integer>>> es) {
		if(!listeSources.contains(eSource)) {
			es.put(eSource, new TreeMap<Character, ArrayList<Integer>>());
			es.get(eSource).put(epsilon, new ArrayList<Integer>());
			es.get(eSource).get(epsilon).add(eActuel);
			listeSources.add(eSource);
		}else {
			
			liste=new ArrayList<Integer>();
			//epsilon transition
			es.put(eSource, es.get(eSource));
			liste.add(eActuel);
			if(es.get(eSource).containsKey(epsilon) && !liste.contains(es.get(eSource).get(epsilon).iterator().next())) {
				liste.addAll(es.get(eSource).get(epsilon));
			}else {
				for(int i=0;i<listeXhar.size();i++) {
					if(es.get(eSource).containsKey(listeXhar.get(i))) {
						
					}
				}
			}
		
			es.get(eSource).put(epsilon, liste);
		}
	}

	/***
	 * cette methode permet d'ajouter dans notre structure de données une  transition qui consomme un caractere
	 * @param eSource:source de la transition
	 * @param eActuel: la destination de la transition
	 * @param es:notre structure de données qui represente notre NFA
	 */
	public static void charTransitionAdd(RegExTree tree,Integer eSource,Integer eActuel, Map<Integer, Map<Character, ArrayList<Integer>>> es) {
		if(!listeSources.contains(eSource)) {	//epsilon transition
			es.put(eSource, new TreeMap<Character, ArrayList<Integer>>());
			es.get(eSource).put((char)tree.root, new ArrayList<Integer>());
			es.get(eSource).get((char)tree.root).add(eActuel);
			listeSources.add(eSource);
			listeXhar.add((char)tree.root);
		}/***/
		else if(listeSources.contains(eSource) && es.get(eSource).containsKey(epsilon)) {
			//es.put(eSource, new TreeMap<Character, ArrayList<Integer>>());
			es.get(eSource).put((char)tree.root, new ArrayList<Integer>());
			es.get(eSource).get((char)tree.root).add(eActuel);
			listeSources.add(eSource);
			listeXhar.add((char)tree.root);
		}/***/else {
			//epsilon transition
			es.put(eSource, es.get(eSource));
			liste.add(eActuel);
			liste.addAll(es.get(eSource).get((char)tree.root));
			es.get(eSource).put((char)tree.root, liste);
			listeXhar.add((char)tree.root);
		}

	}
}
