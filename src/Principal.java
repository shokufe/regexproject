import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Map.Entry;
import java.io.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * cette classe represente notre classe d'execution
 * @author kderb,Simab
 *
 */
public class Principal {
	static final int CONCAT = 0xC04CA7;
	static final int ETOILE = 0xE7011E; 
	static final int ALTERN = 0xA17E54;
	static final int PROTECTION = 0xBADDAD;

	static final int PARENTHESEOUVRANT = 0x16641664;
	static final int PARENTHESEFERMANT = 0x51515151;
	static final int DOT = 0xD07;//3335
    public static String regEx;
    public static String fileName="56667-0";
		/**
	 * represente la structure de données de notre NFA
	 */
	static Map<Integer, Map<Character, ArrayList<Integer>>> ep = new HashMap<Integer, Map<Character,ArrayList<Integer>>>();
	/**
	 * represente les etats finaux de notre DFA
	 */
	static ArrayList<Integer> finals=new ArrayList<Integer>(); 

	public static void main(String args[]) throws FileNotFoundException {
		//On utilise de start et end time pour calculer le temps d'execution de programme
		long start = System.currentTimeMillis();
		if (args.length == 0) {
            return;
        }else if (args.length == 2) {
                regEx = args[0];
                fileName = args[1];
                System.out.println(" >> regEx : " + regEx);
                System.out.println(" >> file name : " + fileName);
            }    
		//cree l'arbre depuis son regex
		System.out.println("*****************RegexTree*************");
		RegEx regex= new RegEx();
		RegExTree tree= regex.getInput();
		System.out.println(" created tree : "+tree);
		//System.out.println();
		//calculer l'automate non determinisme
		System.out.println("****************non-Determinism**************");
		//NFAFinal nfa=new NFAFinal();
		NFA2 nfa=new NFA2();
		Automate autom=nfa.callMain(tree,ep);
		for (int i = 0; i < ep.size(); i++) {
			Map<Character, ArrayList<Integer>> state=ep.get(i);
			for(Entry<Character, ArrayList<Integer>> entry : ep.get(i).entrySet()) {
				for(int j=0;j<entry.getValue().size();j++)
					System.out.print(i+"=="+entry.getKey()+"==>"+entry.getValue().get(j)+",");
				
			}
			if(i==7) {
				System.out.println(" ");
			}
			
		}
		System.out.println(" ");
		//calculer l'automate determinisme
		System.out.println("*****************determinism************");
		Map<ArrayList<Integer>, ArrayList<Map<Character,ArrayList<Integer>>>> newDFA=new HashMap<ArrayList<Integer>, ArrayList<Map<Character,ArrayList<Integer>>>>();
		//DFAKah dfa=new DFAKah();
		DFA2 dfa=new DFA2();
		dfa.setautomateDFA(ep);
		System.out.println("transitions"+dfa.transitions);
		System.out.println("finalsDFA : "+dfa.finalsForMatching);
		
		
		// faire le pattern matching avec l'automate determinisme et le regex arriver en entree
		System.out.println("+++++++++++++++pattern matching+++++++++++++++++");
		
	
		//System.out.println(fname1);
		String fname= "./src/"+fileName+".txt";
		File file = new File(fname); 
		System.out.println(file);
		 Scanner sc = new Scanner(file); 
		 int line=0;
		 int occurenceNumber=0;
		 while (sc.hasNextLine()) {
			 line++;
			 PatternM ptm=new PatternM();
			 //System.out.println(sc.nextLine());
			 
			 //ptm.checkPattern(sc.nextLine(),autom,dfa.transitions);
			if(ptm.checkPattern(sc.nextLine(),autom,dfa.transitions)) {
				 occurenceNumber++;
			 }else {
				// System.out.println("not find");
			 }
			
		 }
		
		 System.out.println("#############################");
		 //fin d'execution : afficher des ligne et le temps d'execution
		 System.out.println("I've parsed ===> "+line+" line" );
		 System.out.println("number of lines that I see Regex is :"+occurenceNumber);
		 
		 long end = System.currentTimeMillis();
		 NumberFormat formatter = new DecimalFormat("#0.00000");
		 System.out.print("Execution time is " + formatter.format((end - start) / 10000d) + " second");
	}

}
