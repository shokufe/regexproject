
import java.util.ArrayList;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;


import java.util.Set;


public class PatternM {
	static Map<ArrayList<Integer>, Boolean> finals=new HashMap<ArrayList<Integer>, Boolean>();
	static Map<ArrayList<Integer>, Boolean> startpoint=new HashMap<ArrayList<Integer>, Boolean>();
	ArrayList< Map<ArrayList<Character>,ArrayList<Integer>>> tmp=new ArrayList<Map<ArrayList<Character>,ArrayList<Integer>>>();
	static ArrayList<Integer> etatSource=new ArrayList<Integer>();
	static ArrayList<Integer> etatActuel=new ArrayList<Integer>();
	static ArrayList<Integer> etatActuelLocal=new ArrayList<Integer>();
	static ArrayList<Integer> helpActuel=new ArrayList<Integer>();
	static ArrayList<Integer> etatSourcelocal=new ArrayList<Integer>();
	static ArrayList<Integer> helpSource=new ArrayList<Integer>();
	static ArrayList<Character> keyEtateActuel=new ArrayList<Character>();
	static ArrayList<Character> firstkeyEtateActuel=new ArrayList<Character>();
	static boolean etoile=false;
	static final char FINAL = '&';
	static boolean findinaltern=false;
	static int decalage=0;
	static Set<ArrayList<Character>> allchar;
	public PatternM(){
		DFA2 g=new DFA2();
		firstkeyEtateActuel=new ArrayList<Character>();
		etatSource=new ArrayList<Integer>();
		keyEtateActuel=new ArrayList<Character>();
		finals.putAll(g.finalsForMatching);
		
		startpoint.putAll(g.StartPoint);
		for (Entry<ArrayList<Integer>, Boolean> entry : startpoint.entrySet()) {
			etatSource.addAll(entry.getKey());
		}
	}
	
	public static boolean checkPattern(String st,Automate autom, Map<ArrayList<Integer>, ArrayList<Map<ArrayList<Character>, ArrayList<Integer>>>> autFinal) {	
		etatActuel=new ArrayList<Integer>();
		if(autFinal.get(etatSource)!=null) {
			for(Entry<ArrayList<Character>, ArrayList<Integer>> entry : autFinal.get(etatSource).get(0).entrySet()) {
				etatActuel.addAll(entry.getValue());
				keyEtateActuel.addAll(entry.getKey());
			}
		}
		firstkeyEtateActuel.addAll(keyEtateActuel);
		char[]c = st.toCharArray();//les characters de text
		for(int i=0;i<c.length;i++) {
			if(checkexistance(c,i,autFinal)==true) {
				System.out.println(st);
				return true;
			}else {
				keyEtateActuel=new ArrayList<Character>();
				keyEtateActuel.addAll(firstkeyEtateActuel);
			}
		}
		return false;
	}
	
	private static boolean checkexistance(char[] c, int i, Map<ArrayList<Integer>, ArrayList<Map<ArrayList<Character>, ArrayList<Integer>>>> autFinal) {
		etatSourcelocal=new ArrayList<Integer>();
		etatActuelLocal=new ArrayList<Integer>();
		etatActuelLocal.addAll(etatActuel);
		etatSourcelocal.addAll(etatSource);

		ArrayList<Integer> tmp =new ArrayList<Integer>();
		for(int j=i;j<c.length;j++) {
			//check if the the char is match with char in the actuel transition
			if(c[j]==keyEtateActuel.get(0)) {
				//check etoile
				if(finals.get(etatActuelLocal)) {
					return true;
				}else {
					if(!checkStateEtoile(autFinal)) {
						newState(autFinal);
						
					}else {
						etoile=true;
					}
					
					continue;
				}
				
			}else if(c[j]!=keyEtateActuel.get(0)&& etoile) {
				newStateetoile(autFinal);
				etoile=false;
				if(c[j]==keyEtateActuel.get(0)) {
					if(finals.get(etatActuelLocal)) {
						return true;
					}else {
						continue;
					}
				}
			}
			//****************************************************************************
			//****************************************************************************
			//****************************************************************************
			//****************************************************************************
			//if this char is not match with transition we will search in all of its childerns
		
			else if(c[j]!=keyEtateActuel.get(0)&&!etoile) {
				boolean test=false;
				int size= autFinal.get(etatSourcelocal).size();
				for(int h=0;h<=size;h++) {
					if(c[j]==keyEtateActuel.get(0))
					{
						if(finals.get(etatActuelLocal)) {
							return true;
						}else {
							newState(autFinal);
							test=true;
							tmp.addAll(etatActuelLocal);
							break;
						}
					}
				    if(c[j]!=keyEtateActuel.get(0))	{
				    	if(h==size)
				    		return false;
				    	else {
				    		helpActuel=new ArrayList<Integer>();
				    		helpActuel.addAll(etatActuelLocal);
				    		helpSource=new ArrayList<Integer>();
				    		helpSource.addAll(etatSourcelocal);
				    		//System.out.println(etatSource+", "+autFinal.get(etatSourcelocal).get(h));
				    		for(Entry<ArrayList<Character>, ArrayList<Integer>> entry : autFinal.get(etatSourcelocal).get(h).entrySet()) {
				    			etatActuelLocal=new ArrayList<Integer>();
				    			etatActuelLocal.add(entry.getValue().get(0));
				    			keyEtateActuel=new ArrayList<Character>();
				    			keyEtateActuel.add(entry.getKey().get(0));
				    		}
				    	}
					}
				}//end for h
				
				if(test) {
					findinaltern=true;
					continue;
				}else {
					return false;
				}
			}
		}
		return false;
	}
	private static boolean checkStateEtoile(
			Map<ArrayList<Integer>, ArrayList<Map<ArrayList<Character>, ArrayList<Integer>>>> autFinal) {
		if(etatActuelLocal.equals(etatSourcelocal))
			return true;
		else {
			if(autFinal.get(etatActuelLocal)!=null) {
				for(int k=0;k<autFinal.get(etatActuelLocal).size();k++) {
					
					for(Entry<ArrayList<Character>, ArrayList<Integer>> entry : autFinal.get(etatActuelLocal).get(k).entrySet()) {
						if(entry.getValue()!=null)
							if(entry.getValue().equals(etatSourcelocal)) {
								return true;
							}
					}
				}
			}
		}//end else
		return false;
	}
	//dans le cas de etoile on reste dans l'etat source donc 
	private static void newStateetoile(Map<ArrayList<Integer>, ArrayList<Map<ArrayList<Character>, ArrayList<Integer>>>> autFinal) {
		if(autFinal.get(etatActuelLocal).get(1)!=null) {
			for(Entry<ArrayList<Character>, ArrayList<Integer>> entry : autFinal.get(etatActuelLocal).get(1).entrySet()) {
				etatActuelLocal=new ArrayList<Integer>();
				etatActuelLocal.addAll(entry.getValue());
				keyEtateActuel=new ArrayList<Character>();
				keyEtateActuel.addAll(entry.getKey());
			}
		}
			
	}

	private static void newState(Map<ArrayList<Integer>, ArrayList<Map<ArrayList<Character>, ArrayList<Integer>>>> autFinal) {
		
		etatSourcelocal=new ArrayList<Integer>();
		etatSourcelocal.addAll(etatActuelLocal);
		if(autFinal.get(etatActuelLocal).get(0)!=null) {
			if(etatSource.contains(etatActuelLocal))
				System.out.println(" &&&&&&"+autFinal.get(etatActuelLocal).get(0));
			for(Entry<ArrayList<Character>, ArrayList<Integer>> entry : autFinal.get(etatActuelLocal).get(0).entrySet()) {
				etatActuelLocal=new ArrayList<Integer>();
				etatActuelLocal.addAll(entry.getValue());
				
				keyEtateActuel=new ArrayList<Character>();
				keyEtateActuel.addAll(entry.getKey());
			}
		}
	}
}
