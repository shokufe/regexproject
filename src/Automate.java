/**
 * cette classe represente notre automate
 * qui est la base des deux autres automates DFA ET NFA
 * @author kderb, simab
 *
 */

public class Automate {
	int eInitial;
	int eFinal;
	Integer eSource;
	Integer eActuel;
	char transition;
	public Automate() {
		this.eInitial=0;
		this.eActuel=0;
		this.eSource=0;
	}
	public int geteActuel() {
		return this.eActuel;
	}
	public void seteFinal(int eFinal) {
		this.eFinal=eFinal;
	}
	/**
	 * cette methode permet de creer un nouvel etat 
	 */
	public void newstate() {
		this.eSource=this.eActuel;
		 this.eActuel++;
		
		 
	}
	/**
	 * cette etat représente l'état Initiale de notre automate
	 * @return
	 */
	public int geteInitial() {
		return eInitial;
	}
	public void seteInitial(int eInitial) {
		this.eInitial = eInitial;
	}
	public Integer geteSource() {
		return eSource;
	}
	public void seteSource(Integer eSource) {
		this.eSource = eSource;
	}
		/**
	 * cette derniere représente la transition de notre automate 
	 * soit une epsilon ou nn
	 * @return
	 */
	public char getTransition() {
		return transition;
	}
	public void setTransition(char transition) {
		this.transition = transition;
	}
	/**
	 * cette methode permet de rendre l'etat final de notre automate
	 * @return
	 */
	public int geteFinal() {
		return eFinal;
	}
	public void seteActuel(Integer eActuel) {
		this.eActuel = eActuel;
	}
	/**
	 * cette méthode permet de se deplacer d'un etat a son successeur
	 */
	public void successor() {
		this.eActuel++;
	}
	
}
