# Clone de egrep avec support partiel des ERE

*Projet réalisé par :* 

   * Kahina  DERBENE
   * Shokoufeh  AHMADI SIMAB


*Requirement :*\
    -Java\
*Execution :*\
   - Java -jar principal.jar

*Explication :*\

   Notre projet consiste à proposer un clone de la commande egrep qui permet d'isoler rapidement un élément dans un fichier de       configuration, dans la sortie d'une commande ou dans un fichier texte quelconque.  

*Les different partie :*\

1. Les Classes : 
- [ class Principale ] : : cette classe contient le main de projet et ça vas appeler les autres class
- [ class Regex ] : creation d'arbre de notre expression regulier
- [ class NFA2 ] : création de l'automate NFA 
- [ class DFA2 ] : création de l'automate DFA      
- [ class PatternM ]: C'est la classe qui fait pattern Matching
- [ class Automate ]: cette classe aide les classe NFA et DFA pour creer et parcourir l'automate
2. Les fichiers texte :
   Il y a quelques fichiers de texte pour tester notre algorithme.

	

    

